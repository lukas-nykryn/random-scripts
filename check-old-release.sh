#!/bin/bash

# Simple script for moving bugs from one RHEL version to another in Bugzilla for the whole team
#
# Copyright (C) 2015 Tomas Hozza
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

usage()
{
    echo
    echo "USAGE:"
    echo "$0 <version> [--move-to-next] [--dry-run] [--user USER]"
    echo "The version is a version of RHEL to check,"
    echo "consisting of <major_version>.<minor_version> (e.g. 6.7)."
    exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

MOVE_BUGS=0
DRY_RUN=0
TEAM=

# check CLI options
VERSION="$1"
shift

if [[ ! $VERSION =~ ^[0-9]+\.[0-9]+$ ]]; then
   echo "ERROR version is not correct"
   usage
fi

while [ "$1" != "${1##--}" ]; do
    case "$1" in
        --move-to-next)
            MOVE_BUGS=1
            shift
            ;;

        --dry-run)
            DRY_RUN=1
            shift
            ;;

        --user)
            TEAM="$2 $TEAM"
            shift 2
            ;;

        --user=?*)
            TEAM="$TEAM ${1#--branch=}"
            shift
            ;;

        *)
            echo "Unrecognized option $option"
            usage
            ;;
    esac
done

TEAM_FILE="team.txt"
# check that the file exists and is readable
if [ -r "$TEAM_FILE" ]; then
    TEAM="$TEAM`cat $TEAM_FILE | tr "\n" " "`"
fi

if [ -z "$TEAM" ]; then
    echo "ERROR: you haven't specified any e-mail adresses"
    exit 1
fi

MAJOR_VERSION="${VERSION%%.*}"
MINOR_VERSION="${VERSION##*.}"

PRODUCT="Red Hat Enterprise Linux $MAJOR_VERSION"
STATE="NEW,ASSIGNED,MODIFIED"

FLAG_OLD="rhel-$VERSION.0?"
FLAG_CLEAR="rhel-$VERSION.0X"
FLAG_NEW="rhel-$MAJOR_VERSION.$(($MINOR_VERSION + 1)).0?"
BUGZILLA_LINK="https://bugzilla.redhat.com/show_bug.cgi?id="

echo "Showing bugs in PRODUCT: '$PRODUCT' with FLAG: '$FLAG_OLD'"
echo "----------------------------------------------------------"

for user in $TEAM; do
    echo
    echo "USER: $user"
    echo "-----------"
    
    CMD="bugzilla query -p \"$PRODUCT\" -a \"$user\" --flag \"$FLAG_OLD\" -t \"$STATE\" --outputformat=\"$BUGZILLA_LINK%{id}	%{status}	%{summary}\""
    
    if [ "$DRY_RUN" -eq "1" ]; then
        echo "Would run:"
        echo $CMD
    else
        eval $CMD
    fi
done

if [ "$MOVE_BUGS" -eq "1" ]; then
    echo
    echo
    echo "Moving bugs in PRODUCT: '$PRODUCT' from '$FLAG_OLD' to '$FLAG_NEW'"
    echo "----------------------------------------------------------------------------------------"

    COMMENT="Moving to $MAJOR_VERSION.$(($MINOR_VERSION + 1)), since it is too late for $MAJOR_VERSION.$MINOR_VERSION."

    for user in $TEAM; do
        echo
        echo "USER: $user"
        echo "-----------"
        
        BUG_IDS="`bugzilla query -p \"$PRODUCT\" -a \"$user\" --flag \"$FLAG_OLD\" -t \"$STATE\" --outputformat=\"%{id}\" | tr \"\n\" \" \"`"
        echo "Bug IDs: $BUG_IDS"

        for id in $BUG_IDS; do
            CMD="bugzilla modify -f \"$FLAG_CLEAR\" -f \"$FLAG_NEW\" --comment=\"$COMMENT\" -p \"$id\""

            if [ "$DRY_RUN" -eq "1" ]; then
                echo "Would run:"
                echo $CMD
            else
                eval $CMD
            fi
        done
    done
fi

